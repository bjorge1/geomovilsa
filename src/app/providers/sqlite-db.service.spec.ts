import { TestBed } from '@angular/core/testing';

import { SqliteDbService } from './sqlite-db.service';

describe('SqliteDbService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SqliteDbService = TestBed.get(SqliteDbService);
    expect(service).toBeTruthy();
  });
});
