import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class SqliteDbService {
  private options: { name: string, location: string, createFromLocation: number} = {
    name: 'data.db',
    location: 'default',
    createFromLocation: 1
  };

  constructor(private platform: Platform, private sqlite: SQLite) { }

  getArticles(): Promise<any> {
    let query = "SELECT title FROM articles";
    return this.query(query, [])
      .then(data => {
        var items = [];
        for (var i = 0; i < data.rows.length; i++) {
          items.push(data.rows.item(i));
        }
        return items;
      })
      .catch(e => {
        console.log('Database Error: ' + JSON.stringify(e));
        return [];
      });
  }

  public createTable(db: SQLiteObject): Promise<void>{
    // You can access to method executeSql now
    return db.executeSql('CREATE TABLE IF NOT EXISTS articles (title VARCHAR(25))', []);
  }
  public query(query, params: any[] = []){
    return this.platform.ready()
      .then(() => {
        return this.sqlite.create(this.options);
      })
      .then((db: SQLiteObject) => {
        return db.executeSql(query, params);
      });
  }
  public queryWithDBCopyPlugin(query, params: any[] = []){
    return this.platform.ready()
      .then(() => {
        return new Promise((resolve, reject) => {
          (window as any).plugins.sqlDB.remove(this.options.name, 0, (status) => {
            (window as any).plugins.sqlDB.copy(this.options.name, 0, (status) => {
              resolve(this.sqlite.create(this.options));
            }, (status) => {
              reject(status);
            });
          }, (status) => {
            reject(status);
          });
        });
      })
      .then((db: SQLiteObject) => {
        return db.executeSql(query, params);
      });
  }
}
