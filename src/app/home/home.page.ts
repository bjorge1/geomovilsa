import { Component, OnInit, OnDestroy } from '@angular/core';
import { Plugins, NetworkStatus, PluginListenerHandle } from "@capacitor/core";
import { NavController } from '@ionic/angular';
import { DashboardComponent } from '../dashboard/dashboard.component';

const { Network} = Plugins;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {
  networkStatus: NetworkStatus;
  networkListener: PluginListenerHandle;

  async ngOnInit(){
    this.networkListener = Network.addListener(
      'networkStatusChange',
      status => {
      console.log('Network status changed', status);
      this.networkStatus = status;
      }
    );
    this.networkStatus = await Network.getStatus();
  }

  ngOnDestroy(): void{
    this.networkListener.remove();
  } 

  constructor(public nav: NavController) {}
  putMapa(){
    this.nav.navigateRoot('/dashboard');
  }
}
