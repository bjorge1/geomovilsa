import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Network } from '@ionic-native/network/ngx';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DataTestComponent } from './data-test/data-test.component';
import { SqliteDbCopy } from '@ionic-native/sqlite-db-copy/ngx';

@NgModule({
  declarations: [AppComponent, DashboardComponent, DataTestComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    SQLite,
    SqliteDbCopy,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(public dbCopy: SqliteDbCopy) {
    this.dbCopy.copy('data.db', 0)
    .then((res: any) => console.log('DB Copiada ' + res))
    .catch((error: any) => console.error(error));
  }
}
