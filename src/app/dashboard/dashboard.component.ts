import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavController } from '@ionic/angular';
import * as leaflet from 'leaflet';
import 'leaflet-draw';
import 'leaflet.locatecontrol';
import 'leaflet-search';
declare const L: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent{
  @ViewChild('map', {static: true}) mapContainer: ElementRef;
  map: any;
  data: any;
  estado: any;
  constructor(public navCtrl: NavController) {

  }

  ionViewDidEnter() {
    this.loadmap();
  }

  loadmap() {
    this.map = leaflet.map('map').setView([-12.0998216, -76.9234648], 13);
    leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Fiscalización',
        maxZoom: 16
      }).addTo(this.map);
    this.map.setZoom(8);
    var guideLayers = new Array();
    var drawnItems = new L.FeatureGroup().addTo(this.map);
    var drawnLocate = new L.control.locate().addTo(this.map);
    
    var drawControl = new L.Control.Draw({
        draw: {
          polyline: { guideLayers: guideLayers },
          marker: { guideLayers: guideLayers },
          polygon: { allowIntersection: false, guideLayers: guideLayers } ,
          rectangle: { showArea: false },
          circle: { guideLayers: guideLayers }
        },
        edit: {
          featureGroup: drawnItems
        }
    });
    
    this.map.addControl( new L.Control.Search({
      url: 'https://nominatim.openstreetmap.org/search?format=json&q={s}',
      jsonpParam: 'json_callback',
      propertyName: 'display_name',
      propertyLoc: ['lat','lon'],
      marker: L.circleMarker([0,0],{radius:30}),
      autoCollapse: true,
      autoType: false,
      minLength: 2
    }) );
  
    this.map.addControl(drawControl);
    this.map.on(L.Draw.Event.CREATED, function (event) {
    const layer = event.layer;

    drawnItems.addLayer(layer);
    this.data = drawnItems.toGeoJSON();
    console.log(this.data);
    drawnLocate.start();
    });

    // Feature Geolocation
    var strLatLng = function(latlng) {
      return "<div class='ion-text-center'><label>("+ latlng.lat.toFixed(4) +", "+ latlng.lng.toFixed(4) +")</label><ion-button color='secondary'><a style='text-decoration:none; color:white' href='home'>FB</a></ion-button></div>"
      ;
    };
    let getPopupContent = function(layer) {
      // Marker - add lat/long
      if (layer instanceof L.Marker || layer instanceof L.CircleMarker) {
          return "Punto creado en " + strLatLng(layer.getLatLng());
      // Circle - lat/long, radius
      } else if (layer instanceof L.Circle) {
          var center = layer.getLatLng(),
              radius = layer.getRadius();
          return "Center: " + strLatLng(center) + "<br />"
                +"Radius: " + radius +" m";
      // Rectangle/Polygon - area
      } else if (layer instanceof L.Polygon) {
          var latlngs = layer._defaultShape ? layer._defaultShape() : layer.getLatLngs(),
              area = L.GeometryUtil.geodesicArea(latlngs);
          return "Area: "+ L.GeometryUtil.readableArea(area, true);
      // Polyline - distance
      } else if (layer instanceof L.Polyline) {
          var latlngs = layer._defaultShape ? layer._defaultShape() : layer.getLatLngs(),
              distance = 0;
          if (latlngs.length < 2) {
              return "Distance: N/A";
          } else {
              for (var i = 0; i < latlngs.length - 1; i++) {
                  distance += latlngs[i].distanceTo(latlngs[i + 1]);
              }
              return "Distance: "+ distance +" m";
          }
      }
      return null;
    };

    this.map.on(L.Draw.Event.CREATED, function(event) {
      var layer = event.layer;
      var content = getPopupContent(layer);
      if (content !== null) {
          layer.bindPopup(content);
      }
      drawnItems.addLayer(layer);
    });
    
    const map = this.map;
    // Web location
    
    map.locate({ setView: true});

    // When we have a location draw a marker and accuracy circle
    function onLocationFound(e) {
    const radius = (e.accuracy / 2).toFixed(1);

    leaflet.marker(e.latlng).addTo(map)
             .bindPopup('Estás dentro de los ' + radius + ' metros desde este punto').openPopup();

    leaflet.circle(e.latlng, radius).addTo(map);
    }
    map.on('locationfound', onLocationFound);

    // Alert on location error
    function onLocationError(e) {
      alert(e.message);
    }

    this.map.on('locationerror', onLocationError);
  }
  test(){
    this.navCtrl.navigateRoot('/test');
  }

    MostrarLatLon(){
    let map = this.map;
    let popup = L.popup();

    function onMapClick(e) {
      popup
        .setLatLng(e.latlng)
        .setContent("Hiciste clic en el mapa en " + e.latlng.toString())
        .openOn(map);
    }
    if(this.estado == 1){
      map.on('click', onMapClick);
      this.estado = 0;
    }
    else{
      map.off('click');
      this.estado = 1;
    }
  }

  
}
