import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { SqliteDbService } from '../providers/sqlite-db.service';

@Component({
  selector: 'app-data-test',
  templateUrl: './data-test.component.html',
  styleUrls: ['./data-test.component.scss'],
})
export class DataTestComponent implements OnInit {
  public items: Array<any> = [];
  constructor(public navCtrl: NavController,
              private platform: Platform,
              private dbProvider: SqliteDbService) { 
    this.platform.ready().then(() => {
      // call openDB method
      this.dbProvider.getArticles().then((data) => {
        console.log(JSON.stringify(data));
        this.items = data;
      });
    });
  }

  ngOnInit() { }
}
